#ifndef STACK_H
#define STACK_H

#include <stdbool.h>

// Opaque Stack object.
typedef struct Stack Stack;

// Value type for the stack.
typedef int StackValue;

// Makes a new stack. Returns NULL if not able to create.
Stack* Stack_New();

// Relinquishes a stack, and fixes dangling pointer.
void Stack_Delete(Stack** stack);

// Pushes value on the stack. It will be the next value returned in pop (unless
// another value is pushed on of course).
void Stack_Push(Stack* stack, StackValue value);

// Whether stack is empty.
bool Stack_Empty(Stack* stack);

// Pops the last value off the stack, and returns it.
StackValue Stack_Pop(Stack* stack);

#endif
