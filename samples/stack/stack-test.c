#include "stack.h"
#include "../unit-test.h"

int main() {
  // perform test multiple time to guard against flakiness (if undefined
  // behavior, for example).
  for (int test_num = 0; test_num < 10; ++test_num) {
    Stack* stack = Stack_New();
    AssertTrue(Stack_Empty(stack));

    Stack_Push(stack, 10);
    AssertTrue(!Stack_Empty(stack));
    Stack_Push(stack, 20);
    AssertTrue(!Stack_Empty(stack));
    AssertTrue(Stack_Pop(stack) == 20);
    AssertTrue(Stack_Pop(stack) == 10);
    AssertTrue(Stack_Empty(stack));

    // stress test
    for (int i = 0; i < 1000; ++i) {
      Stack_Push(stack, i);
    }
    for (int i = 999; i >=0; --i) {
      AssertTrue(Stack_Pop(stack) == i);
    }
    AssertTrue(Stack_Empty(stack));

    Stack_Delete(&stack);
  }
  PrintTestSummary();
}
