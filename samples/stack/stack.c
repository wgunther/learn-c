#include <stdlib.h>

#include "stack.h"

// StackNode things: this is an internal helper class.

typedef struct StackNode StackNode; // forward declaration.

struct StackNode {
  StackValue data;
  StackNode* next;
};

static StackNode* StackNode_New(StackValue data) {
  StackNode* node = malloc(sizeof(StackNode));
  node->data = data;
  node->next = NULL;
  return node;
}

static void StackNode_Delete(StackNode** node) {
  free(*node);
  *node = NULL;
}

// Stack things.

struct Stack {
  StackNode* head;
};

Stack* Stack_New() {
  Stack* stack = malloc(sizeof(Stack));
  if (!stack) {
    return stack;
  }
  stack->head = NULL;
  return stack;
};

void Stack_Delete(Stack** stack) {
  StackNode* head = (*stack)->head;
  while (head != NULL) {
    StackNode* next = head->next;
    StackNode_Delete(&head);
    head = next;
  }
  free(*stack);
}

void Stack_Push(Stack* stack, StackValue value) {
  StackNode* new_node = StackNode_New(value);
  new_node->next = stack->head;
  stack->head = new_node;
}

StackValue Stack_Pop(Stack* stack) {
  StackNode* old_node = stack->head;
  StackValue value = old_node->data;
  stack->head = old_node->next;
  StackNode_Delete(&old_node);
  return value;
}

bool Stack_Empty(Stack* stack) {
  return stack->head == NULL;
}
