#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "vector.h"

static bool verbose = 0;
static long num_tests_passed;
static long num_tests_failed;

#define ArraySize(array) sizeof(array) / sizeof(*array)

#define TestVectorEquals(vec, ...) \
  do { \
    int equals[] = {__VA_ARGS__}; \
    TestVectorEqualsHelper(vec, equals, ArraySize(equals)); \
  } while(0)

#define AssertTrue(test) \
  do { \
    if (test) { \
      if (verbose) { \
        printf("Test passed: " #test "\n"); \
      } \
      ++num_tests_passed; \
    } else { \
      if (verbose) { \
        printf("Test failed: " #test "\n"); \
      } \
      ++num_tests_failed; \
    } \
  } while (0)

void TestPushBack(Vector* vec, int value, size_t before_size) {
  AssertTrue(Vector_Size(vec) == before_size);
  Vector_PushBack(vec, value);
  AssertTrue(*(Vector_At(vec, before_size)) == value);
  AssertTrue(Vector_Size(vec) == before_size + 1);
}

void TestPopBack(Vector* vec, int value, size_t before_size) {
   AssertTrue(Vector_Size(vec) == before_size);
   AssertTrue(Vector_PopBack(vec) == value);
   AssertTrue(Vector_Size(vec) == before_size - 1);
}

void TestVectorEqualsHelper(Vector* vec, int* values, size_t size) {
  AssertTrue(Vector_Size(vec) == size);
  for (size_t i = 0; i < size; ++i) {
    AssertTrue(*(Vector_At(vec, i)) == values[i]);
  }
}

int main() {
  // loop to test flakiness
  for (int i = 0; i < 5; ++i) {
    Vector* vec = Vector_New();

    // basic tests: pushback, popback
    TestPushBack(vec, 10, 0);
    TestPushBack(vec, 9, 1);
    TestPushBack(vec, 8, 2);
    TestVectorEquals(vec, 10, 9, 8);
    TestPopBack(vec, 8, 3);
    TestVectorEquals(vec, 10, 9);
    TestPopBack(vec, 9, 2);
    TestVectorEquals(vec, 10);
    TestPopBack(vec, 10, 1);

    // resize
    const int kZero = 0;
    const int kOne = 1;
    const int kNumZero = 100;
    const int kNumOne = 1000;
    Vector_Resize(vec, kNumZero, &kZero);
    Vector_Resize(vec, kNumOne + kNumZero, &kOne);
    for (int i = 0; i < kNumZero; ++i) {
      AssertTrue(*Vector_At(vec, i) == 0);
    }
    for (int i = 0; i < kNumOne; ++i) {
      AssertTrue(*Vector_At(vec, i + kNumZero) == 1);
    }

    // clear
    Vector_Resize(vec, 0, NULL);
    TestVectorEquals(vec);

    // test data
    const int kNumToStress = 1000000;
    for (int i = 0; i < kNumToStress; ++i) {
      Vector_PushBack(vec, i);
    }
    int* data = Vector_Data(vec);
    for (int i = 0; i < kNumToStress; ++i) {
      AssertTrue(data + i == Vector_At(vec, i));
      AssertTrue(data[i] == i);
    }

    Vector_Delete(&vec);
    AssertTrue(vec == NULL);
  }

  printf("Passed: %ld\nFailed: %ld\n", num_tests_passed, num_tests_failed);
}

