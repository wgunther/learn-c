#ifndef VECTOR_H
#define VECTOR_H

#include <stdlib.h>

typedef struct Vector Vector;
typedef int Vector_Value;

// Allocates new vector. Returns opaque pointer to it. Must be passed into
// Vector_Delete for clean-up when done.
Vector* Vector_New();

// Destroys vector allocated with Vector_New. Takes in a pointer to the pointer
// returned by Vector_New. Sets it to NULL.
void Vector_Delete(Vector** container);

// Get size of vector.
size_t Vector_Size(const Vector* container);

// Add value to end of vector. Vector may resize if needed.
void Vector_PushBack(Vector* container, Vector_Value value);

// Remove value from end of vector. Returns the value.
Vector_Value Vector_PopBack(Vector* container);

// Retrieve pointer to the value at a given index.
Vector_Value* Vector_At(Vector* container, size_t index);

// Resize vector. If enlarging, default_value must point to a value which
// will be copied in all new spots. If shrinking, default_value is ignored.
void Vector_Resize(Vector* container, size_t new_size,
    const Vector_Value* default_value);

// Get underlying data. PushBack or Resize invalidates the pointer returned.
Vector_Value* Vector_Data(Vector* container);

#endif  // VECTOR_H
