#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "vector.h"

#define Warn(...) fprintf(stderr, __VA_ARGS__)
#define Die(code, ...) Warn(__VA_ARGS__), exit(code)

struct Vector {
  size_t size;
  size_t capacity;
  Vector_Value* buffer;
};

static void* MallocOrDie(size_t size) {
  void* buffer = malloc(size);
  if (!buffer) {
    Die(1, "Could not allocate memory\n");
  }
  return buffer;
}

static void* ReallocOrDie(void* buffer, size_t size) {
  void* new_buffer = realloc(buffer, size);
  if (!new_buffer) {
    free(buffer);
    Die(1, "Could not allocate memory during realloc\n");
  }
  return new_buffer;
}

static void GrowIfNecessary(Vector* container, size_t size_required) {
  const double kGrowthFactor = 1.5;
  if (size_required > container->capacity) {
    const size_t new_capacity = kGrowthFactor * size_required;
    container->buffer = ReallocOrDie(container->buffer,
        new_capacity * sizeof(Vector_Value));
    container->capacity = new_capacity;
  }
}


Vector* Vector_New() {
  const size_t kInitialSize = 10;
  Vector* vector = MallocOrDie(sizeof(Vector));
  vector->buffer = MallocOrDie(kInitialSize * sizeof(Vector_Value));
  vector->size = 0;
  vector->capacity = kInitialSize;
  return vector;

}
void Vector_Delete(Vector** container) {
  free((*container)->buffer);
  free(*container);
  *container = NULL;
}

size_t Vector_Size(const Vector* container) {
  return container->size;
}
void Vector_PushBack(Vector* container, Vector_Value value) {
  const size_t new_size = container->size + 1;
  GrowIfNecessary(container, new_size);
  assert(container->capacity >= new_size);
  container->buffer[new_size - 1] = value;
  container->size = new_size;
}

Vector_Value Vector_PopBack(Vector* container) {
  const size_t old_size = container->size;
  assert(old_size > 0);
  container->size = old_size - 1;
  return container->buffer[old_size - 1];
}

Vector_Value* Vector_At(Vector* container, size_t index) {
  assert(index < container->size);
  return &container->buffer[index];
}

void Vector_Resize(Vector* container, size_t new_size,
    const Vector_Value* default_value) {
  const size_t old_size = container->size;
  if (new_size <= old_size) {
    container->size = new_size;
  } else {
    GrowIfNecessary(container, new_size);
    assert(container->capacity >= new_size);
    container->size = new_size;
    for (size_t new_element_index = old_size;
         new_element_index < new_size;
         ++new_element_index) {
      container->buffer[new_element_index] = *default_value;
    }
  }
}

Vector_Value* Vector_Data(Vector* container) {
  return container->buffer;
}
