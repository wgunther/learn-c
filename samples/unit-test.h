#ifndef UNIT_TEST_H
#define UNIT_TEST_H

#include <stdio.h>
#include <stdbool.h>

extern long num_tests_passed;
extern long num_tests_failed;
extern bool verbose_test;


#define AssertTrue(test) \
  do { \
    if (test) { \
      if (verbose_test) { \
        printf("Test passed: " #test "\n"); \
      } \
      ++num_tests_passed; \
    } else { \
      if (verbose_test) { \
        printf("Test failed: " #test "\n"); \
      } \
      ++num_tests_failed; \
    } \
  } while (0)

#define PrintTestSummary() \
    printf("Total passed: %ld\nTotal failed: %ld", \
        num_tests_passed, num_tests_failed)

#endif
